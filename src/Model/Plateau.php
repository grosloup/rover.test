<?php

namespace App\Model;

use App\Interfaces\iPlateau;
use App\Interfaces\iPosition;


class Plateau implements iPlateau
{
    protected $width;
    
    protected $height;
    
    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function isPositionValid(iPosition $position): bool
    {
        return $position->getX() >= 0 && 
            $position->getX() <= $this->width && 
            $position->getY() >= 0 && 
            $position->getY() <= $this->height
        ;
    }
}
