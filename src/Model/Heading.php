<?php

namespace App\Model;


use App\Interfaces\iHeading;

class Heading implements iHeading
{

    protected $facing;

    public function __construct($initial)
    {
        $this->facing = $initial;
    }

    public function rotateLeft()
    {
        switch ($this->facing) {
                case Heading::HEADING_NORTH:
                    $this->facing = Heading::HEADING_WEST;
                    break;
                case Heading::HEADING_EAST:
                    $this->facing = Heading::HEADING_NORTH;
                    break;
                case Heading::HEADING_SOUTH:
                    $this->facing = Heading::HEADING_EAST;
                    break;
                case Heading::HEADING_WEST:
                    $this->facing = Heading::HEADING_SOUTH;
                    break;
            }
    }

    public function rotateRight()
    {
        switch ($this->facing) {
                case Heading::HEADING_NORTH:
                    $this->facing = Heading::HEADING_EAST;
                    break;
                case Heading::HEADING_EAST:
                    $this->facing = Heading::HEADING_SOUTH;
                    break;
                case Heading::HEADING_SOUTH:
                    $this->facing = Heading::HEADING_WEST;
                    break;
                case Heading::HEADING_WEST:
                    $this->facing = Heading::HEADING_NORTH;
                    break;
            }
    }

    public function asString(): string
    {
        return $this->facing;
    }
}
