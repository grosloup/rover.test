<?php

namespace App\Model;


use App\Interfaces\iHeading;
use App\Interfaces\iInstruction;
use App\Interfaces\iPlateau;
use App\Interfaces\iPosition;
use App\Interfaces\iRover;

class Rover implements iRover
{
    protected $plateau;
    
    protected $position;
    
    protected $heading;
    
    
    public function __construct(iPlateau $plateau, iPosition $position, iHeading $heading)
    {
        $this->plateau = $plateau;
        $this->position = $position;
        $this->heading = $heading;
    }

    public function getPosition(): iPosition
    {
        return $this->position;
    }

    public function getHeading(): iHeading
    {
        return $this->heading;
    }
    
    public function setDebug(bool $debug) 
    {
        
    }

    function action(iInstruction $Instruction)
    {
        if ($Instruction->equal(Instruction::ACTION_MOVE)) {
            switch ($this->heading->asString()) {
                case Heading::HEADING_NORTH:
                    $oldY = $this->position->getY();
                    $newY = $oldY+1;
                    $this->position->setPosition($this->position->getX(), $newY);
                    if(!$this->plateau->isPositionValid($this->position)) {
                        $this->position->setPosition($this->position->getX(), $this->plateau->getHeight());
                    }
                    break;
                
                case Heading::HEADING_EAST:
                    $oldX = $this->position->getX();
                    $newX = $oldX+1;
                    $this->position->setPosition($newX, $this->position->getY());
                    if(!$this->plateau->isPositionValid($this->position)) {
                        $this->position->setPosition($this->plateau->getWidth(), $this->position->getY());
                    }
                    break;
                               
                case Heading::HEADING_SOUTH:
                    $oldY = $this->position->getY();
                    $newY = $oldY-1;
                    $this->position->setPosition($this->position->getX(), $newY);
                    if(!$this->plateau->isPositionValid($this->position)) {
                        $this->position->setPosition($this->position->getX(), 0);
                    }
                    break;
                case Heading::HEADING_WEST:
                    $oldX = $this->position->getX();
                    $newX = $oldX-1;
                    $this->position->setPosition($newX, $this->position->getY());
                    if(!$this->plateau->isPositionValid($this->position)) {
                        $this->position->setPosition(0, $this->position->getY());
                    }
                    break;
                

                
            }
            
        } elseif ($Instruction->equal(Instruction::ACTION_TURN_LEFT)) {
            $this->heading->rotateLeft();
        } else {
            $this->heading->rotateRight();
        }
    }
    
    

}
