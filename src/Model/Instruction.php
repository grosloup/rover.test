<?php

namespace App\Model;

use App\Interfaces\iInstruction;

class Instruction implements iInstruction
{

    protected $initial;
    
    public function __construct($initial)
    {
        $this->initial = $initial;
    }

    public function isValid($command): bool
    {
        return $command === self::ACTION_MOVE || $command === self::ACTION_TURN_LEFT || $command === self::ACTION_TURN_RIGHT;
    }

    public function equal($string): bool
    {
        return $this->initial === $string;
    }

    public function asString(): string
    {
        return $this->initial;
    }

}
