<?php

namespace App\Model;

use App\Interfaces\iPosition;

class Position implements iPosition
{
    /**
     * @var int
     */
    protected $x;
    /**
     * @var int
     */
    protected $y;
    
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    function setPosition($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
        
        return $this;
    }

    function getX()
    {
        return $this->x;
    }

    function getY()
    {
        return $this->y;
    }

}
